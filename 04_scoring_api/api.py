#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
import datetime
import logging
import hashlib
import uuid
import re
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler
import scoring

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class CharField(object):
    validate_exp = r'.*[a-z]+.*$'

    def __init__(self, required=False, nullable=True):
        self.required = required
        self.nullable = nullable
        self.val = None

    def __set_name__(self, owner, name):
        self._name = name

    def __get__(self, instance, owner):
        return self.val

    def validator(self, value):
        return bool(re.match(self.validate_exp, str(value)))

    def __set__(self, instance, value):
        if self.required and value is None:
            raise TypeError(self._name)
        elif not self.nullable and value == '':
            raise ValueError(self._name)
        elif value != '' and not self.validator(value) and value is not None:
            raise ValueError(self._name)
        else:
            self.val = value


class ArgumentsField(CharField):
    def validator(self, value):
        return type(value) == dict


class EmailField(CharField):
    validate_exp = r'^[a-zA-Z0-9\_\-\!.]{3,}@[a-zA-Z0-9\_\-]+\.[a-zA-Z]+'


class PhoneField(CharField):
    validate_exp = r'^7[0-9]{10}$'


class DateField(CharField):
    def validator(self, value):
        try:
            datetime.datetime.strptime(str(value), '%d.%m.%Y')
            return True
        except:
            return False


class BirthDayField(CharField):
    def validator(self, value):
        try:
            res = (datetime.datetime.now() - datetime.datetime.strptime(str(value), '%d.%m.%Y')).days < 25550
        except:
            return False
        return res


class GenderField(CharField):
    def validator(self, value):
        return value in [0, 1, 2]


class ClientIDsField(CharField):
    def validator(self, value):
        return type(value) == list and len([x for x in value if type(x) != int]) == 0 and len(value) > 0


class MethodClass(object):

    def __init__(self, req_body):
        self.code = None
        self.response = {}
        try:
            classfields = [f for f in self.__class__.__dict__ if f[0] != '_' and f not in ['results', 'is_admin',
                                                                                           'code', 'response',
                                                                                           'get_metod']]
            for field in classfields:
                self.__setattr__(field, req_body.get(field))

        except TypeError as e:
            self.code = INVALID_REQUEST
            self.response = {'error': f'{e} is required but None'}
        except ValueError as e:
            self.code = INVALID_REQUEST
            self.response = {'error': f'{e} value is incorrect'}


class ClientsInterestsRequest(MethodClass):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)

    def results(self):
        if self.code:
            return self.response, self.code, None
        for cid in self.client_ids:
            self.response.update({str(cid): scoring.get_interests(None, cid)})
        self.code = 200
        return self.response, self.code, {'nclients': str(len(self.client_ids))}


class OnlineScoreRequest(MethodClass):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def results(self):
        if self.code:
            return self.response, self.code, None
        classfields = [f for f in self.__class__.__dict__ if f[0] != '_' and f not in ['results', 'is_admin',
                                                                                       'code', 'response',
                                                                                       'get_metod']]
        f = []
        for field in classfields:
            if getattr(self, field, None) is not None:
                f.append(field)
        if not(all(x in f for x in ['phone', 'email'])
            or all(x in f for x in ['first_name', 'last_name'])
            or all(x in f for x in ['gender', 'birthday'])):
            logging.info(f'{self.phone} and {self.email} = {bool(self.phone and self.email)}')
            logging.info(f'{self.first_name} and {self.last_name} = {bool(self.first_name and self.last_name)}')
            logging.info(f'{self.gender} and {self.birthday} = {bool(self.gender and self.birthday)}')
            return {'error': 'too few arguments'}, INVALID_REQUEST, None
        self.response = {"score": scoring.get_score(None, self.phone,
                                          self.email, self.birthday, self.gender,
                                          self.first_name, self.last_name)}
        self.code = 200
        return self.response, self.code, {'has': f}


class MethodRequest(MethodClass):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    def results(self):
        if not self.code and check_auth(self):
            self.response = {"method": self.method}
            self.code = 200
        elif not self.code:
            self.response = '{"error":"Forbidden"}'
            self.code = 403
        return self.response, self.code

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode('utf-8')).hexdigest()
    else:
        digest = hashlib.sha512((request.account + request.login + SALT).encode('utf-8')).hexdigest()
    if request.token == digest:
        return True
    return False


def method_handler(request, ctx, store):
    r = MethodRequest(request['body'])
    response, code = r.results()
    if code != 200:
        return response, code
    else:
        method = r.method
        req_args = request['body']["arguments"]
    if method == 'online_score':
        a = r.is_admin
        r = OnlineScoreRequest(req_args)
        response, code, ads = r.results()
        if a and response.get('score'):
            response['score'] = 42
    elif method == 'clients_interests':
        r = ClientsInterestsRequest(req_args)
        response, code, ads = r.results()
    else:
        response = {"error": ERRORS[INVALID_REQUEST]}
        code = INVALID_REQUEST
    if code == 200:
        ctx.update(ads)
    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except Exception as E:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode('utf-8'))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
