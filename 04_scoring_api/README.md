## Python scoring API
####request example:
```json
 {"account": "<company name>", "login": "<username>", "method": "<requested method>", "token":
"<auth token>", "arguments": {<args_disct>}}
```
fields list:
* account - string, not req, nullable
* login - string, req, nullable
* method - string, req, nullable
* token - string, req, nullable
* arguments - json object, req, nullable (accepted methods: clients_interests and online_score)
####online score request argument object example
```json
{"phone": "74952048586", "email": "somemail@somedomain.local", "first_name": "vasiliy", "last_name": "pupkin", "birthday": "01.01.1990", "gender": 1}
```
fields list:
* phone - string, 11 numbers, starts from 7, not req, nullable
* email - string, email string, not req, nullable 
* first_name - string, not req, nullable
* last_name - same
* birthday - string DD.MM.YYYY, not req, nullable
* gender - int in [1-2], not req, nullable

resp example
```json
{"response": {"score": 3.0}, "code": 200}
```
#### client interests request argument object exmaple
```json
{"client_ids": [1, 2], "date": "19.07.2017"}
```
fields list:
* client_id - list of int, req, not nullable
* date - string DD.MM.YYYY, not req, nullable

resp example:
```json
{"response": {"1": ["cinema", "books"], "2": ["pets", "geek"]}, "code": 200}
```
